// ==UserScript==
// @name         Tribal wars Scavenger, Builder and recruiter
// @namespace    http://tampermonkey.net/
// @version      1.2
// @description  Tribal wars bot
// @author       Eric Kavalec
// @match        https://*.divokekmeny.cz/*
// @grant        none
// ==/UserScript==

//*************************** SETUP ***************************//
// Choose Minimum and maximum wait time between actions (in milliseconds)
const MIN_WAIT_TIME = 20000;
const MAX_WAIT_TIME = 40000;
// Choose whether you want the bot to queue buildings in the defined order (= true) or
// as soon as a building is available for the building queue (= false)
const WAIT_FOR_ORDER_BUILDINGS = true

//*************************** /SETUP ***************************//

// Constants
const OVERVIEW_VIEW = "OVERVIEW_VIEW";
const HEADQUARTERS_VIEW = "HEADQUARTERS_VIEW";
const SCAVENGE_VIEW = "SCAVENGE_VIEW";
const BARRACKS_VIEW = "BARRACKS_VIEW";
const MARKET_VIEW = "MARKET_VIEW";
const SCAVENGE_PAGE = "/game.php?screen=place&mode=scavenge";
const BARRACKS_PAGE = "/game.php?screen=barracks";
const OVERVIEW_PAGE = "/game.php?screen=overview";
const HEADQUARTERS_PAGE = "/game.php?screen=main";
const MARKET_PAGE = "/game.php?screen=market&mode=exchange";
var carry_dict = {"archer": 10,"axe": 10,"heavy": 50,"knight": 100,"light": 80,"marcher": 50,"spear": 25,"sword": 15};


let delay = Math.floor(Math.random() * (MAX_WAIT_TIME - MIN_WAIT_TIME) + MIN_WAIT_TIME);

(function() {
    'use strict';

    console.log("-- Tribal Wars script enabled --");
    // PHASE 1 = building + scavenging + recruiting + claimingRewards

    PHASE1()

    //--------------------------------------------------------------------------------------

    // PHASE 2 = building + scavenging + selling

    //PHASE2()

})();

function PHASE2(){
    let delay = Math.floor(Math.random() * (MAX_WAIT_TIME - MIN_WAIT_TIME) + MIN_WAIT_TIME);
    let currentView = getCurrentView();
    console.log(currentView);
    setTimeout(function(){
        if (currentView == HEADQUARTERS_VIEW){
            headquarters(SCAVENGE_PAGE)
        } else if (currentView == SCAVENGE_VIEW){
            scavenge(MARKET_PAGE)
        } else if (currentView == MARKET_VIEW){
            market(HEADQUARTERS_PAGE)
        }
    }, delay);
}

function PHASE1(){
    let currentView = getCurrentView();
    console.log(currentView);
    setTimeout(function(){
        if (currentView == HEADQUARTERS_VIEW){
            headquarters(SCAVENGE_PAGE);
        }
        else if (currentView == SCAVENGE_VIEW){
            scavenge(BARRACKS_PAGE);

        } else if (currentView == BARRACKS_VIEW){
            barracks(OVERVIEW_PAGE);

        }else if (currentView == OVERVIEW_VIEW){
            overview(HEADQUARTERS_PAGE);

        }
    }, delay);
}
function headquarters(nextPage){
    console.log("building")
    var interval_hq = setInterval(function(){
        buildNextBuilding();
        var time_till_nextRound = new Date().getTime() - localStorage.getItem("scavenge_finish_time")
        console.log(time_till_nextRound/1000/60)
        if(time_till_nextRound > 0){
            window.location.href = nextPage;
        }
    }, 10000);
}

function overview(nextPage){
    console.log("reward collection")
    claimRewards()
    setTimeout(function(){
        window.location.href = nextPage;
    }, delay)
}

function barracks(nextPage){
    var total_spears = Number(document.querySelector("#train_form > table > tbody > tr:nth-child(2) > td:nth-child(3)").textContent.split("/")[1]);

    if(document.getElementById("spear_0_afford_hint").style.display != "none" || total_spears > 900){
        console.log("nejsou suroviny nebo je > 900 kopí")
        setTimeout(function(){
            window.location.href = nextPage;
        },3000)
        return
    }
    if(total_spears > 200){
        localStorage.setItem("twcheese.userConfig",'{"props":{"ASS":{"troopsAssigner":{"mode":"addict","allowedOptionIds":[2,3,4,1],"targetDurationSeconds":7200,"troops":{"spear":{"maySend":true,"reserved":0},"sword":{"maySend":true,"reserved":0},"axe":{"maySend":false,"reserved":0},"light":{"maySend":false,"reserved":30},"heavy":{"maySend":true,"reserved":0},"knight":{"maySend":true,"reserved":0}},"troopOrder":[["axe","light","marcher"],["spear","sword","archer"],["heavy"],["knight"]]}}}}')
    }
    var queued = 0;
    if(typeof(document.getElementsByClassName("lit-item")[0]) != "undefined"){
        if(document.getElementsByClassName("lit-item")[0].textContent.includes("Kopiník")){
            queued += Number(document.getElementsByClassName("lit-item")[0].textContent.match(/\d+/)[0])
        }
    }
    if(document.getElementById("trainqueue_barracks") != null){
        var trainQueue = document.getElementById("trainqueue_barracks").children
        for(var i =0;i<trainQueue.length-1;i++){
            queued += Number(trainQueue[i].children[0].textContent.match(/\d+/)[0])
        }
    }
    console.log(queued)
    if(queued < 10){
        var max_spears = Number(document.querySelector("#spear_0_a").textContent.match(/\d+/)[0])
        if(max_spears < 10){
            document.querySelector("#spear_0").value = max_spears;
            setTimeout(function(){
                document.getElementsByClassName("btn btn-recruit")[0].click()
            },1000)
        } else {
            document.querySelector("#spear_0").value = 10;
            setTimeout(function(){
                document.getElementsByClassName("btn btn-recruit")[0].click()
            },1000)
        }
    }

    setTimeout(function(){
        window.location.href = nextPage;
    },30000)
}

function market(nextPage){
    var merch = Number(document.getElementById("market_merchant_available_count").textContent)
    console.log(merch)
    var wood = Number(document.getElementById("wood").textContent);
    var stone = Number(document.getElementById("stone").textContent);
    var iron = Number(document.getElementById("iron").textContent);
    if(merch > 0){
        console.log(wood, stone, iron)
        if(wood > 1000 || stone > 1000 || iron > 1000){
            var max = Math.max(wood,stone,iron)
            var to_sell = (max == wood) ? "wood" :(max == stone) ? "stone" :(max == iron) ? "iron" : "none";
            var r = Math.floor(eval(to_sell)/1000)
            var resources = r >= merch ? merch*1000 - 300 : r * 1000 - 300
            document.getElementsByName("sell_"+to_sell)[0].value = resources
            setTimeout(function(){
                document.querySelector("#premium_exchange_form > input").click();
                setTimeout(function(){
                    document.querySelector("#premium_exchange > div > div > div.confirmation-buttons > button.btn.evt-confirm-btn.btn-confirm-yes").click()
                    setTimeout(function(){
                        location.reload()
                    },3000)
                },3000)
            },3000)

        } else {
            setTimeout(function(){
                window.location.href = nextPage;
            },30000)
        }
    } else {
        setTimeout(function(){
            window.location.href = nextPage;
        },30000)
    }
}

function scavenge(nextPage){
    try{
        if(document.getElementsByClassName("return-countdown").length > 0){
            var timeLeft = document.getElementsByClassName("return-countdown")[document.getElementsByClassName("return-countdown").length-1].textContent.split(":")
            var timeLeftParsed = (Number(timeLeft[0])*3600+ Number(timeLeft[1])*60+ Number(timeLeft[2]))*1000 + 300000+ Math.floor(Math.random() * 120000)
            localStorage.setItem("scavenge_finish_time", new Date().getTime()+timeLeftParsed)
            window.location.href = nextPage;
        }
        console.log("scavenging")
        var max_troops = document.getElementsByClassName("units-entry-all squad-village-required");
        var run_buttons = document.getElementsByClassName("btn btn-default free_send_button");
        var usableIds = scrapeUsableOptionIds()
        var total_carry = getTotalCarry(max_troops);
        var scav1 = false;
        var scav2 = false;
        var scav3 = false;
        var scav4 = false;

        for(var i =0;i<usableIds.length;i++){
            switch(usableIds[i]){
                case 1:
                    scav1 = true
                    break;
                case 2:
                    scav2 = true
                    break;
                case 3:
                    scav3 = true
                    break;
                case 4:
                    scav4 = true
                    break;
            }
        }

        console.log(scav1,scav2,scav3,scav4)

        var scav1_r = 0;
        var scav2_r = 0;
        var scav3_r = 0;
        var scav4_r = 0;

        if(scav1 && !scav2 && !scav3 && !scav4){
            scav1_r = 1;
        }

        if(scav1 && scav2 && !scav3 && !scav4){
            scav1_r = 0.79;
            scav2_r = 0.21;
        }

        if(scav1 && scav2 && scav3 && !scav4){
            scav1_r = 0.625;
            scav2_r = 0.25;
            scav3_r = 0.125;
        }

        if(scav1 && scav2 && scav3 && scav4){
            scav1_r = 0.57;
            scav2_r = 0.23;
            scav3_r = 0.115;
            scav4_r = 0.077;
        }
        var values = [];
        values[0] = [];
        values[1] = [];
        values[2] = [];
        values[3] = [];
        for (var j = 0; j < max_troops.length; j++) {
            var troop = max_troops[j].innerText.match(/\d+/g)[0];
            values[0].push(Math.floor(troop * scav1_r));
            values[1].push(Math.floor(troop * scav2_r));
            values[2].push(Math.floor(troop * scav3_r));
            values[3].push(Math.floor(troop * scav4_r));
        }
        console.log(values)
        sendScavenge(values,usableIds)
        setTimeout(function(){
            var timeLeft = document.getElementsByClassName("return-countdown")[document.getElementsByClassName("return-countdown").length-1].textContent.split(":")
            var timeLeftParsed = (Number(timeLeft[0])*3600+ Number(timeLeft[1])*60+ Number(timeLeft[2]))*1000 + 300000+ Math.floor(Math.random() * 120000)
            localStorage.setItem("scavenge_finish_time", new Date().getTime()+timeLeftParsed)
            window.location.href = nextPage;
        },30000)
    } catch(e){
        console.log(e)
        setTimeout(function(){
            window.location.href = nextPage;
        },30000)
    }
}

function sendScavenge(values,usableIds){
    var event = new Event('change');
    var text_boxes = document.getElementsByClassName("unitsInput input-nicer");
    for(var i=0;i<usableIds.length;i++){
        setTimeout(function(i){
            for (var j = 0; j < text_boxes.length; j++) {
                //console.log(i)
                console.log(values[usableIds[i]-1][j])
                text_boxes[j].value = values[usableIds[i]-1][j];
                text_boxes[j].dispatchEvent(event);
                setTimeout(function(i){
                    document.querySelector("#scavenge_screen > div > div.options-container > div:nth-child("+usableIds[i]+") > div.status-specific > div > div.action-container > a.btn.btn-default.free_send_button").click()
                },2000,i)
            }
        },5000 + i*5000,i)
        //usableIds[i]
    }
}

function getCurrentView(){
    let currentUrl = window.location.href;
    if (currentUrl.endsWith('overview') || currentUrl.endsWith('overview#')){
        return OVERVIEW_VIEW;
    }
    else if (currentUrl.endsWith('main') || currentUrl.endsWith('main#')){
        return HEADQUARTERS_VIEW;
    }
    else if (currentUrl.endsWith('scavenge') || currentUrl.endsWith('scavenge#')){
        return SCAVENGE_VIEW;

    } else if (currentUrl.endsWith('barracks') || currentUrl.endsWith('barracks#')){
        return BARRACKS_VIEW;

    } else if (currentUrl.endsWith('exchange') || currentUrl.endsWith('exchange#')){
        return MARKET_VIEW;
    }
}

function buildNextBuilding(){
    let nextBuildingElement = getNextBuildingElement();
    if (nextBuildingElement !== undefined){
        nextBuildingElement.click();
        console.log("Clicked on " + nextBuildingElement);
    }
}

function getNextBuildingElement() {
    let buildableBuildings = document.getElementsByClassName("btn btn-build");
    let buildingElementsQueue = getBuildingElementsQueue();
    let found;
    while(found === undefined && buildingElementsQueue.length > 0){
        var next = buildingElementsQueue.shift();
        if (buildableBuildings.hasOwnProperty(next)){
            let nextBuilding = document.getElementById(next);
            var isVisible = nextBuilding.offsetWidth > 0 || nextBuilding.offsetHeight > 0;
            if (isVisible){
                found = nextBuilding;
            }
            if (WAIT_FOR_ORDER_BUILDINGS){
                console.log("waiting for " + next)
                break;
            }
        }
    }
    return found;
}

function getTotalCarry(max_troops){
    var temp = 0;
    for (var i = 0; i < max_troops.length; i++) {
        var troop = max_troops[i].dataset.unit;
        var troop_count = max_troops[i].innerText.match(/\d+/g)[0];
        temp += carry_dict[troop]*troop_count;
    }
    return temp;
}

function scrapeUsableOptionIds() {
    let usableIds = [];
    $(document).find('.scavenge-option').has('.free_send_button:not(.btn-disabled)').each(function() {
        let [, id] = $(this).find('.portrait').css('background-image').match(/options\/(\d).png/);
        usableIds.push(parseInt(id));
    });

    return usableIds;
}

function claimRewards(){
    document.querySelector("#new_quest").click()
    setTimeout(function(){
        document.querySelector("#popup_box_quest > div > div > div.quest-popup-header > div > ul > li.qline-tab.selected-tab > a").click()
        var interval = setInterval(function(){
            console.log(document.getElementsByClassName("reward-system-claim-button").length)
            if(document.getElementsByClassName("reward-system-claim-button").length > 0 && document.getElementsByClassName("reward-system-claim-button")[0].parentElement.querySelector("span") == null){
                document.getElementsByClassName("reward-system-claim-button")[0].click()
            } else {
                console.log("clearing interval")
                clearInterval(interval)
                return;
            }
        },3000)

        },1000)
}


function getBuildingElementsQueue() {
    var queue = [];
    queue.push("main_buildlink_wood_1");
    queue.push("main_buildlink_stone_1");
    queue.push("main_buildlink_iron_1");
    queue.push("main_buildlink_wood_2");
    queue.push("main_buildlink_stone_2");
    queue.push("main_buildlink_iron_2");
    queue.push("main_buildlink_storage_1");
    queue.push("main_buildlink_wood_3");
    queue.push("main_buildlink_stone_3");
    queue.push("main_buildlink_iron_3");
    queue.push("main_buildlink_storage_2");
    queue.push("main_buildlink_farm_1");
    queue.push("main_buildlink_farm_2");
    queue.push("main_buildlink_main_1");
    queue.push("main_buildlink_main_2");
    queue.push("main_buildlink_main_3");
    queue.push("main_buildlink_barracks_1");
    queue.push("main_buildlink_barracks_2");
    queue.push("main_buildlink_barracks_3");
    queue.push("main_buildlink_storage_3");
    queue.push("main_buildlink_market_1");
    queue.push("main_buildlink_market_2");
    queue.push("main_buildlink_wood_4");
    queue.push("main_buildlink_wood_5");
    queue.push("main_buildlink_stone_4");
    queue.push("main_buildlink_wood_6");
    queue.push("main_buildlink_wood_7");
    queue.push("main_buildlink_stone_5");
    queue.push("main_buildlink_wood_8");
    queue.push("main_buildlink_wood_9");
    queue.push("main_buildlink_stone_6");
    queue.push("main_buildlink_wood_10");
    queue.push("main_buildlink_stone_7");
    queue.push("main_buildlink_iron_4");
    queue.push("main_buildlink_iron_5");
    queue.push("main_buildlink_storage_4");
    queue.push("main_buildlink_storage_5");
    queue.push("main_buildlink_storage_6");
    queue.push("main_buildlink_farm_3");
    queue.push("main_buildlink_farm_4");
    queue.push("main_buildlink_farm_5");
    queue.push("main_buildlink_main_4");
    queue.push("main_buildlink_main_5");
    queue.push("main_buildlink_storage_7");
    queue.push("main_buildlink_storage_8");
    queue.push("main_buildlink_storage_9");
    queue.push("main_buildlink_storage_10");
    queue.push("main_buildlink_storage_11");
    queue.push("main_buildlink_statue_1");
    queue.push("main_buildlink_barracks_4");
    queue.push("main_buildlink_barracks_5");
    queue.push("main_buildlink_farm_6");
    queue.push("main_buildlink_farm_7");
    queue.push("main_buildlink_storage_12");
    queue.push("main_buildlink_wood_11");
    queue.push("main_buildlink_stone_8");
    queue.push("main_buildlink_main_6");
    queue.push("main_buildlink_main_7");
    queue.push("main_buildlink_main_8");
    queue.push("main_buildlink_main_9");
    queue.push("main_buildlink_main_10");
    queue.push("main_buildlink_wood_12");
    queue.push("main_buildlink_stone_9");
    queue.push("main_buildlink_farm_8");
    queue.push("main_buildlink_wood_13");
    queue.push("main_buildlink_storage_13");
    queue.push("main_buildlink_wood_14");
    queue.push("main_buildlink_wood_15");
    queue.push("main_buildlink_stone_10");
    queue.push("main_buildlink_stone_11");
    queue.push("main_buildlink_stone_12");
    queue.push("main_buildlink_stone_13");
    queue.push("main_buildlink_stone_14");
    queue.push("main_buildlink_market_3");
    queue.push("main_buildlink_market_4");
    queue.push("main_buildlink_market_5");
    queue.push("main_buildlink_iron_6");
    queue.push("main_buildlink_iron_7");
    queue.push("main_buildlink_iron_8");
    queue.push("main_buildlink_iron_9");
    queue.push("main_buildlink_iron_10");
    queue.push("main_buildlink_wall_1");
    queue.push("main_buildlink_wall_2");
    queue.push("main_buildlink_wall_3");
    queue.push("main_buildlink_wall_4");
    queue.push("main_buildlink_wall_5");
    queue.push("main_buildlink_wood_16");
    queue.push("main_buildlink_stone_15");
    queue.push("main_buildlink_iron_11");
    queue.push("main_buildlink_farm_9");
    queue.push("main_buildlink_farm_10");
    queue.push("main_buildlink_smith_1");
    queue.push("main_buildlink_smith_2");
    queue.push("main_buildlink_smith_3");
    queue.push("main_buildlink_smith_4");
    queue.push("main_buildlink_smith_5");
    queue.push("main_buildlink_stable_1");
    queue.push("main_buildlink_stable_2");
    queue.push("main_buildlink_stable_3");
    queue.push("main_buildlink_stable_4");
    queue.push("main_buildlink_stable_5");
    queue.push("main_buildlink_storage_14");
    queue.push("main_buildlink_storage_15");
    queue.push("main_buildlink_farm_11");
    queue.push("main_buildlink_farm_12");
    queue.push("main_buildlink_farm_13");
    queue.push("main_buildlink_wood_17");
    queue.push("main_buildlink_stone_16");
    queue.push("main_buildlink_iron_12");
    queue.push("main_buildlink_wood_18");
    queue.push("main_buildlink_stone_17");
    queue.push("main_buildlink_wood_19");
    queue.push("main_buildlink_stone_18");
    queue.push("main_buildlink_wood_20");
    queue.push("main_buildlink_stone_19");
    queue.push("main_buildlink_stone_20");
    queue.push("main_buildlink_storage_16");
    queue.push("main_buildlink_storage_17");
    queue.push("main_buildlink_main_11");
    queue.push("main_buildlink_main_12");
    queue.push("main_buildlink_main_13");
    queue.push("main_buildlink_main_14");
    queue.push("main_buildlink_main_15");
    queue.push("main_buildlink_main_16");
    queue.push("main_buildlink_main_17");
    queue.push("main_buildlink_main_18");
    queue.push("main_buildlink_main_19");
    queue.push("main_buildlink_main_20");
    queue.push("main_buildlink_wall_6");
    queue.push("main_buildlink_wall_7");
    queue.push("main_buildlink_wall_8");
    queue.push("main_buildlink_wall_9");
    queue.push("main_buildlink_wall_10");
    queue.push("main_buildlink_storage_18");
    queue.push("main_buildlink_storage_19");
    queue.push("main_buildlink_storage_20");
    queue.push("main_buildlink_farm_14");
    queue.push("main_buildlink_farm_15");
    queue.push("main_buildlink_farm_16");
    queue.push("main_buildlink_farm_17");
    queue.push("main_buildlink_barracks_6");
    queue.push("main_buildlink_barracks_7");
    queue.push("main_buildlink_barracks_8");
    queue.push("main_buildlink_barracks_9");
    queue.push("main_buildlink_barracks_10");
    queue.push("main_buildlink_barracks_11");
    queue.push("main_buildlink_barracks_12");
    queue.push("main_buildlink_barracks_13");
    queue.push("main_buildlink_barracks_14");
    queue.push("main_buildlink_barracks_15");
    queue.push("main_buildlink_iron_13");
    queue.push("main_buildlink_iron_14");
    queue.push("main_buildlink_iron_15");
    queue.push("main_buildlink_iron_16");
    queue.push("main_buildlink_iron_17");
    queue.push("main_buildlink_iron_18");
    queue.push("main_buildlink_iron_19");
    queue.push("main_buildlink_iron_20");
    queue.push("main_buildlink_stable_6");
    queue.push("main_buildlink_stable_7");
    queue.push("main_buildlink_stable_8");
    queue.push("main_buildlink_stable_9");
    queue.push("main_buildlink_stable_10");
    queue.push("main_buildlink_smith_6");
    queue.push("main_buildlink_smith_7");
    queue.push("main_buildlink_smith_8");
    queue.push("main_buildlink_smith_9");
    queue.push("main_buildlink_smith_10");
    queue.push("main_buildlink_garage_1");
    queue.push("main_buildlink_garage_2");
    queue.push("main_buildlink_garage_3");
    queue.push("main_buildlink_garage_4");
    queue.push("main_buildlink_garage_5");
    queue.push("main_buildlink_market_6");
    queue.push("main_buildlink_market_7");
    queue.push("main_buildlink_market_8");
    queue.push("main_buildlink_market_9");
    queue.push("main_buildlink_market_10");
    queue.push("main_buildlink_wall_11");
    queue.push("main_buildlink_wall_12");
    queue.push("main_buildlink_wall_13");
    queue.push("main_buildlink_wall_14");
    queue.push("main_buildlink_wall_15");
    queue.push("main_buildlink_storage_21");
    queue.push("main_buildlink_storage_22");
    queue.push("main_buildlink_storage_23");
    queue.push("main_buildlink_storage_24");
    queue.push("main_buildlink_storage_25");
    queue.push("main_buildlink_market_11");
    queue.push("main_buildlink_market_12");
    queue.push("main_buildlink_market_13");
    queue.push("main_buildlink_market_14");
    queue.push("main_buildlink_market_15");
    queue.push("main_buildlink_farm_18");
    queue.push("main_buildlink_farm_19");
    queue.push("main_buildlink_farm_20");
    queue.push("main_buildlink_wood_21");
    queue.push("main_buildlink_stone_21");
    queue.push("main_buildlink_iron_21");
    queue.push("main_buildlink_wood_22");
    queue.push("main_buildlink_stone_22");
    queue.push("main_buildlink_iron_22");
    queue.push("main_buildlink_wood_23");
    queue.push("main_buildlink_stone_23");
    queue.push("main_buildlink_iron_23");
    queue.push("main_buildlink_wood_24");
    queue.push("main_buildlink_stone_24");
    queue.push("main_buildlink_iron_24");
    queue.push("main_buildlink_wood_25");
    queue.push("main_buildlink_stone_25");
    queue.push("main_buildlink_iron_25");
    queue.push("main_buildlink_barracks_16");
    queue.push("main_buildlink_barracks_17");
    queue.push("main_buildlink_barracks_18");
    queue.push("main_buildlink_barracks_19");
    queue.push("main_buildlink_barracks_20");
    queue.push("main_buildlink_wood_26");
    queue.push("main_buildlink_stone_26");
    queue.push("main_buildlink_iron_26");
    queue.push("main_buildlink_wood_27");
    queue.push("main_buildlink_stone_27");
    queue.push("main_buildlink_iron_27");
    queue.push("main_buildlink_storage_26");
    queue.push("main_buildlink_storage_27");
    queue.push("main_buildlink_storage_28");
    queue.push("main_buildlink_storage_29");
    queue.push("main_buildlink_storage_30");
    queue.push("main_buildlink_wood_28");
    queue.push("main_buildlink_stone_28");
    queue.push("main_buildlink_iron_28");
    queue.push("main_buildlink_wood_29");
    queue.push("main_buildlink_stone_29");
    queue.push("main_buildlink_iron_29");
    queue.push("main_buildlink_wood_30");
    queue.push("main_buildlink_stone_30");
    queue.push("main_buildlink_iron_30");
    queue.push("main_buildlink_market_16");
    queue.push("main_buildlink_market_17");
    queue.push("main_buildlink_market_18");
    queue.push("main_buildlink_market_19");
    queue.push("main_buildlink_market_20");
    queue.push("main_buildlink_market_21");
    queue.push("main_buildlink_market_22");
    queue.push("main_buildlink_market_23");
    queue.push("main_buildlink_market_24");
    queue.push("main_buildlink_market_25");
    return queue;
}